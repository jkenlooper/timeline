# Timeline

A simple web application to mutate a timeline.  **It's almost like time
traveling, but without all the fluff.**

This uses `chill` as the server end and stores the data in a sqlite db.
AngularJS is used on the front end.  I'm just learning AngularJS and wanted
a simple and fun project to play around with.

*WIP* It looks rather ugly and plain at the moment. Features that it does now:

* Add events to the timeline.
* Events in the timeline are sorted by their timestamp.

## Setup instructions for the server end

To get this crazy contraption running involves having a server that can serve
up the front end resources and web pages. The server will need to be configured
to have a few routes that send and retrieve data as JSON.  I'm just using
[chill][] for this and my setup steps are detailed here.

1.  Clone this repository and cd to it.
1.  `chill operate` will show the operate menu.
1.  Initialize a new database by going to -> "chill.database functions" -> "init_db"
1.  Insert a node with the name 'timeline_page' and no value. -> "insert_node"
1.  Insert a route for that node with the path '/'. -> "insert_route"
1.  Set a template for that node to be 'timeline.html'. -> "add_template_for_node"

Now the bare minimum has been set up to display a webpage at the '/' route in
the browser.  Now configure the '/api/events/' endpoint that will accept GET
and POST requests with JSON data.

2.  Insert a node with the name 'GET_api_events' and no value. -> "insert_node"
2.  Insert a route for that node with the path '/api/events/'. -> "insert_route"
2.  Insert the 'select_events.sql' query for that node. -> "insert_query"
2.  Insert a node with the name 'POST_api_events' and no value. -> "insert_node"
2.  Insert a route for that node with the path '/api/events/' and set the method to 'POST'. -> "insert_route"
2.  Insert the 'insert_event.sql' query for that node. -> "insert_query"
2.  Create the 'Event' database table by executing the 'create_table_event.sql'. -> "execute sql file"

Exit out of the operate menu by not selecting any menu item and just hitting
return.  The 'db' file is the sqlite database file and can be edited with
whatever SQL tools you may have.  The operate script simply makes maintaining
this database a little easier as there are a few specific things that chill
uses.  The final step is starting the web application on your local machine.

3.   `chill run` to start the server in the foreground.
3.  Visit <http://localhost:5000/> with a browser to see the web application running.


[chill]: https://github.com/jkenlooper/chill
