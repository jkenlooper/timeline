#!/bin/bash

# Sync source files to build server
rsync --archive --progress --itemize-changes \
  .babelrc \
  package.json \
  queries \
  README.md \
  site.cfg \
  src \
  templates \
  webpack.config.js \
  jake@pi:/home/jake/dev/timeline/

# Sync built files back to local
rsync --archive --progress --itemize-changes \
  --delete-after \
  jake@pi:/home/jake/dev/timeline/{db.dump,dist,node_modules,npm-debug.log} \
  ./

