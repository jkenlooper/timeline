var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var postcssImport = require('postcss-import')
var postcssNested = require('postcss-nested')
var postcssCustomProperties = require('postcss-custom-properties')
var postcssCustomMedia = require('postcss-custom-media')
var postcssCalc = require('postcss-calc')
var postcssUrl = require('postcss-url')
var autoprefixer = require('autoprefixer')
var cssByebye = require('css-byebye')
var cssnano = require('cssnano')

/* Not needed for now.
var ExtractHTML = new ExtractTextPlugin('[name].html', {
  allChunks: true
})
*/
var ExtractCSS = new ExtractTextPlugin('[name].css', {
  allChunks: true
})

module.exports = function makeWebpackConfig () {

  /**
   * Config
   * Reference: http://webpack.github.io/docs/configuration.html
   * This is the object where all configuration gets set
   */
  var config = {}

  /**
   * Entry
   * Reference: http://webpack.github.io/docs/configuration.html#entry
   */
  config.entry = {
    timelinepage: './src/index.js'
  }

  /**
   * Output
   * Reference: http://webpack.github.io/docs/configuration.html#output
   */
  config.output = {
    // Absolute output directory
    path: __dirname + '/dist',

    // Filename for entry points
    filename: '[name].bundle.js'
  }

  /* TODO: not needed? */
  /*
  config.resolve = {
    modulesDirectories: ['src', 'node_modules']
  }
  */

  // Initialize module
  config.module = {
    preLoaders: [],
    loaders: [
      // JS LOADER
      // Reference: https://github.com/babel/babel-loader
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
      // HTML LOADER
      // Reference: https://github.com/webpack/raw-loader
      // Allow loading html through js
        test: /\.html$/,
        loader: 'raw'
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=[name].[ext]'
      },
      {
        test: /\.(png|svg|gif|jpg)$/,
        loader: 'file-loader'
      },
      {
      // CSS LOADER
      // Reference: https://github.com/webpack/css-loader
      // Allow loading css through js
      //
      // Reference: https://github.com/postcss/postcss-loader
      // Postprocess your css with PostCSS plugins
        test: /\.css$/,
      // Reference: https://github.com/webpack/style-loader
        loader: ExtractCSS.extract('style-loader', 'css-loader!postcss-loader')
      },
      {
        test: /\.svg$/,
        loader: 'file-loader!svgo-loader',
        exclude: /(node_modules|fonts)/
      }
    ]
  }

  config.plugins = [
    // Reference: http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
    // Only emit files when there are no errors
    new webpack.NoErrorsPlugin(),

    // Reference: http://webpack.github.io/docs/list-of-plugins.html#dedupeplugin
    // Dedupe modules in the output
    new webpack.optimize.DedupePlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'commons',
      minChunks: 2,
      minSize: 2
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    ExtractCSS,
    // ExtractHTML
  ]

  config.postcss = function (webpack) {
    var use = [
      postcssImport({
        addDependencyTo: webpack
      }),
      postcssNested,
      postcssCustomProperties,
      postcssCustomMedia,
      postcssCalc,
      autoprefixer,
      postcssUrl,
      cssByebye({
        rulesToRemove: [
          '.u-textKern',
          '.u-textTruncate',
        ]
      })
    ]
    if (this.minimize) {
      use.push(cssnano({
        safe: true
      }))
    }
    return use
  }

  return config
}()
