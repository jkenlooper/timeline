/*
 * I'm learning AngularJS and most examples and tutorials use es5.  But, es6 is
 * a little cleaner, so this code is a blend of both.  When I have a better
 * grasp of es6 and AngularJS; the code will be refactored using es6.
 *
 * Articles and other projects that I'm using as references:
 * http://angular-tips.com/blog/2015/06/using-angular-1-dot-x-with-es6-and-webpack/
 * https://github.com/preboot/angular-webpack
 */

export default class TimelineCtrl {
  constructor ($scope, Events) {
    // For flashing a message to the user
    $scope.message = ''

    // The title input field value
    $scope.title = ''

    // Options for the datepicker and timepicker
    $scope.dateOptions = {
    }
    $scope.timeOptions = {
      'show-seconds': true
    }

    $scope.getEvents = function () {
      Events.get(function (events, msg) {
        $scope.events = events
        $scope.message = msg

        // Set the eventDatetime to the latest event date or the current date if no events
        let latest = new Date($scope.events[$scope.events.length - 1].timestamp) || new Date()
        $scope.eventDatetime = new Date(latest.getFullYear(), latest.getMonth(), latest.getDate(), latest.getHours(), latest.getMinutes(), latest.getSeconds())
      })
    }

    $scope.addEvent = function () {
      var obj = {
        'timestamp': $scope.eventDatetime,
        'title': $scope.title
      }
      Events.add(obj, function (msg) {
        $scope.events.push(obj)
        $scope.message = msg
        // Reset the title
        $scope.title = ''
      })
    }

    // Initialize by fetching the events from the server.
    $scope.getEvents()
  }
}
TimelineCtrl.$inject = ['$scope', 'Events']
