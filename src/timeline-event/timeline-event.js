/* timelineEvent directive */

import './timeline-event.css'

export default timelineEvent

function timelineEvent () {
  return {
    restrict: 'E',
    // Using webpack to bundle, so no need to use templateUrl
    template: require('./timeline-event.html')
  }
}
