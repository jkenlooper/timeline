export default class Events {
  constructor ($http, $window) {
    this.$http = $http
    this.$window = $window
  }

  get (cb) {
    let events = []
    let msg = ''
    this.$http.get('/api/events/').then(function (response) {
      if (response.data) {
        events = Array.isArray(response.data) ? response.data : [response.data]
        events.forEach(function (e) {
          // Set all timestamp to be Date objects in events
          if (e.timestamp) {
            e.timestamp = new Date(e.timestamp) || ''
          }
        })
        msg = 'Fetched events from server.'
      } else {
        msg = 'No events.'
      }
    }, function (err) {
      msg = `No events: ${err.status} ${err.statusText}`
    }).finally(function () {
      cb(events, msg)
    })
  }

  add (obj, cb) {
    // POST the obj to the server. The callback function is called with
    // a status message.
    let msg = ''
    try {
      this.$http.post('/api/events/', obj).then(function (response) {
        msg = `Added event:  ${response.status} ${response.statusText}`
      }, function (err) {
        if (this.$window.location.host === 'timeline.weboftomorrow.com') {
          msg = 'Event Added.  Not saved to server.'
        } else {
          msg = `Event not saved to server: ${err.status} ${err.statusText}`
        }
      }).finally(function () {
        cb(msg)
      })
    } catch (err) {
      //
    }
  }
}
Events.$inject = ['$http', '$window']
