import angular from 'angular'
import datepicker from 'angular-ui-bootstrap/src/datepicker'
import timepicker from 'angular-ui-bootstrap/src/timepicker'

// Add site-wide css
import './site.css'
import 'bootstrap/dist/css/bootstrap.css'

import TimelineCtrl from './timeline.js'
import Events from './events.js'
import timelineEvent from './timeline-event/timeline-event.js'

// Export the module as a string using the name property.
export default angular.module('timeline', [datepicker, timepicker])
  .controller('TimelineCtrl', TimelineCtrl)
  .service('Events', Events)
  .directive('timelineEvent', timelineEvent)
	.name
